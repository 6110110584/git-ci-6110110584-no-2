# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 23:16:19 2021

@author: hp
"""

import random


def guess_int(start, stop):
    return random.randint(start, stop)

def guess_float(start, stop):
    return random.uniform(start, stop)

print("guess_int",guess_int(0, 10))
print("guess_float",guess_float(0, 10))
